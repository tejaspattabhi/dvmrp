#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define SIZE 120
#define STRSIZE 50

#define LANS 10
#define ROUTERS 10

#define HOP_COUNT 1
#define LAN_ID 0
#define NEXT_HOP 2

#define TRUE 1

struct RoutingTable
{
	int lanId;
	int hopCount;
	int nextHopRouter;
	int forwardTable[LANS]; /*If data is received from lanId*/
};

void paddFixLen (char *message)
{
	while (strlen (message) < SIZE - 1)
		strcat (message, "_");
	strcat (message, "\n");
}

int main (int argc, char *argv[])
{
	int i, j, k, l, childStatus, numChars;
	pid_t termChildPid, DVChildPid, checkRecvPid, NMRUpdatePid, NMRSendPid;

	int shmId1, shmId2, shmId3, shmId4, shmId5;
	int shmIdNMR[LANS], shmIdupdateNMR[LANS];
	
	struct RoutingTable *routTable;
	struct RoutingTable *neighRouters[ROUTERS];
	
	int *recvCount; /*Read by Parent, written by Child*/
	int *updateOnRecvCount; /*Read by Child, written by Parent*/

	int LAN[LANS], routerId, nAssLANs, nAssRouters, ROUTER[ROUTERS];
	int hostLanId, curLanId;
	int rOUT, LANfd[LANS];
	
	int *NMR[LANS]; /*Read by Parent, written by Child*/
	int *updateOnNMR[LANS]; /*Read by Child, written by Parent*/
	int *sendNMR;
	int *parentLAN;
	
	int rSendingNMR;
	int leafRouter;
	
	int prevSent;
	int canSend, forward, shouldISend;
	int garbVal, incRoutId;

	char LANFname[LANS][STRSIZE], routFname[STRSIZE], temp[STRSIZE];
	char med1[STRSIZE], med2[STRSIZE], recvDV[50][STRSIZE];
	char *tokenizer, *dv, *myMessage, *fileMessage;
	
	char *tempFName;
	int tempFd;

	/*Initializations*/
	rOUT = routerId = incRoutId = shmId1 = shmId2 = shmId3 = hostLanId = curLanId = rSendingNMR = leafRouter = shouldISend = -1;
	nAssLANs = prevSent = garbVal = nAssRouters = forward = 0;

	tokenizer = dv = myMessage = fileMessage = NULL;

	canSend = 0;

	dv = (char*) malloc (sizeof (char) * (SIZE + 10));
	myMessage = (char*) malloc (sizeof (char) * (SIZE + 10));
	fileMessage = (char*) malloc (sizeof (char) * (SIZE + 10));
	tempFName = (char*) malloc (sizeof (char) * (SIZE + 50));

	/*Receiver tables*/
	shmId2 = shmget (IPC_PRIVATE, LANS * sizeof (int), IPC_CREAT|0666);
	recvCount = (int *) shmat (shmId2, NULL, 0);

	shmId3 = shmget (IPC_PRIVATE, LANS * sizeof (int), IPC_CREAT|0666);
	updateOnRecvCount = (int *) shmat (shmId3, NULL, 0);
	
	/*NMR Variables*/
	shmId4 = shmget (IPC_PRIVATE, sizeof (int) * LANS, IPC_CREAT|0666);
	sendNMR = (int *) shmat (shmId4, NULL, 0);
	
	shmId5 = shmget (IPC_PRIVATE, sizeof (int) * LANS, IPC_CREAT|0666);
	parentLAN = (int *) shmat (shmId5, NULL, 0);
	
	/*Trying not to complicate :P*/
	for (i = 0; i < LANS; i++)
	{
		shmIdNMR[i] = shmget (IPC_PRIVATE, sizeof (int) * ROUTERS, IPC_CREAT|0666);
		NMR[i] = (int *) shmat (shmIdNMR[i], NULL, 0);
		
		shmIdupdateNMR[i] = shmget (IPC_PRIVATE, sizeof (int) * ROUTERS, IPC_CREAT|0666);
		updateOnNMR[i] = (int *) shmat (shmIdupdateNMR[i], NULL, 0);
	}

	for (i = 0; i < LANS; i++)
	{
		LAN[i] = -1;
		LANfd[i] = -1;
		recvCount[i] = 0;
		updateOnRecvCount[i] = 0;
		parentLAN[i] = -1;
		sendNMR[i] = 0;
		
		for (j = 0; j < ROUTERS; j++)
		{
			NMR[i][j] = -1;
			updateOnNMR[i][j] = -1;
		}
	}
	
	for (i = 0; i < ROUTERS; i++)
		neighRouters[i] = NULL;
	
	if (argc < 3)
	{
		printf ("Invalid set of arguements.. Please execute again with good arguements\n");
		return 1;
	}

	routerId = atoi (argv[1]); /*My Router ID*/

	if (routerId < 0 || routerId > 9)
	{
		printf ("Invalid set of arguements.. Please execute again with good arguements\n");
		return 1;
	}

	for (i = 2; i < argc; i++)
	{
		LAN[i - 2] = atoi (argv[i]); /*Remember!!*/

		if (LAN[i - 2] > 9 || LAN[i - 2] < 0)
		{
			printf ("Invalid set of arguements.. Please execute again with good arguements\n");
			return 1;
		}
		nAssLANs++;
	}

	for (i = 0; i < nAssLANs; i++)
		recvCount[LAN[i]] = 0;

	sprintf(routFname, "rout%d.txt", routerId);
	rOUT = open (routFname, O_CREAT|O_WRONLY|O_TRUNC, 0777);

	for (i = 0; i < nAssLANs; i++)
	{
		sprintf(LANFname[i], "lan%d.txt", LAN[i]);
		LANfd[LAN[i]] = open (LANFname[i], O_RDONLY|O_CREAT, 0777);
		
		if (LANfd[LAN[i]] < 0)
		{
			printf ("Error opening file %s\nAborting!!\n", LANFname[i]);
			return 1;
		}
	}
	
	/*Routing Table*/
	shmId1 = shmget (IPC_PRIVATE, LANS * sizeof (struct RoutingTable), IPC_CREAT|0666);
	routTable = (struct RoutingTable *) shmat (shmId1, NULL, 0);
	
	for (i = 0; i < LANS; i++)
	{
		routTable[i].lanId = i; /*LAN ID*/
		routTable[i].hopCount = 10; /*Hop Count*/ /*Infinity*/
		routTable[i].nextHopRouter = routerId; /*Next Hop*/
		
		for (j = 0; j < LANS; j++)
			routTable[i].forwardTable[j] = 0;
	}
	
	for (i = 0; i < nAssLANs; i++) /*Update the information to my associated LANs*/
	{
		routTable[LAN[i]].nextHopRouter = routerId;
		routTable[LAN[i]].hopCount = 0;
	}

	/*Child to timeline the parent (and all others) for 100s*/
	termChildPid = fork();
	if (termChildPid < 0)
	{
		printf ("Error in forking a new process!\nPlease restart!!\n");
		return 1;
	}
	else if (termChildPid == 0)
	{
		sleep (100); /*This process will run only for 100 seconds!*/
		return 0;
	}
	else
	{	
		/*Parent: Perform regular action of a ROUTER*/
		/*printf ("*******************\nRouter Description:\n");
		printf ("Router ID: %d\n", routerId);
		printf ("ROUT Name and ID: %s and %d\n", routFname, rOUT);
		printf ("LANs Associated: %d\n", nAssLANs);
		for (i = 0; i < nAssLANs; i++)
			printf ("%d\t", LAN[i]);
		printf ("\n*******************\n");*/

		/*Router Implementation*/
		DVChildPid = fork();

		if (DVChildPid < 0)
		{
			printf ("Error in forking a new process!\nPlease restart!!\n");
			return 1;
		}
		else if (DVChildPid == 0) /*Child: This is the process Dedicated for DV transmission!*/
		{
			/*Send DV*/
			while (TRUE)
			{
				for (i = 0; i < nAssLANs; i++)
				{
					sprintf (dv, "DV %d %d ", LAN[i], routerId);
					for (j = 0; j < LANS; j++)
					{
						sprintf (temp, "%d router%d ", routTable[j].hopCount, routTable[j].nextHopRouter);
						strcat (dv, temp);
					}
					paddFixLen (dv);

					write (rOUT, dv, strlen(dv));
				}
				/*Wait for 5 seconds before sending next DV*/
				sleep (5);
			}
		}
		else
		{
			/*Parent: New child to Check for the receivers every 20 Seconds*/
			checkRecvPid = fork();
			
			if (checkRecvPid < 0)
			{
				printf ("Error in forking!\n Aborting..!\n\n");
				return 1;
			}
			else if (checkRecvPid == 0) /*Child dedicated for reciever's check*/
			{
				while (TRUE) /*Parent's order: Till I kill you, keep checking for the receivers in me every 20s*/
				{
					for (i = 0; i < LANS; i++)
					{
						recvCount[i] = updateOnRecvCount[i];
						updateOnRecvCount[i] = 0;
					}
					sleep (20); /*Cycle of 20 seconds*/
				}
			}
			else
			{
				/*Child: To update the NMR table*/
				NMRUpdatePid = fork();
				
				if (NMRUpdatePid < 0)
				{
					printf ("NMR Child forking failed!!!\n Program aborting!!\n");
					return 1;
				}
				else if (NMRUpdatePid == 0)
				{	
					while (TRUE)
					{
						for (i = 0; i < LANS; i++)
						{
							for (j = 0; j < ROUTERS; j++)
							{
								if (NMR[i][j] == 1 && updateOnNMR[i][j] != 1)
								{
									NMR[i][j] = 0;
								}
								updateOnNMR[i][j] = -1;
							}
						}
						sleep (20);
					}
				}
				
				/*NMR sending every 10s*/
				NMRSendPid = fork();
				if (NMRSendPid < 0)
				{
					printf ("NMR Child forking failed!!!\n Program aborting!!\n");
					return 1;
				}
				else if (NMRSendPid == 0)
				{	
					while (TRUE)
					{
						for (i = 0; i < ROUTERS; i++)
						{
							if (sendNMR[i] == 1)
							{
								sprintf (fileMessage, "NMR %d %d %d ", parentLAN[i], routerId, i);
								paddFixLen (fileMessage);
								//printf ("NMR: %s\n", fileMessage);
								if (parentLAN[i] < 0)
								{
									i--;
									continue;
								}
								write (rOUT, fileMessage, SIZE);
							}
						}
						sleep (10);
					}
				}
				/*Parent action continues!*/
				
				/*Parent: Check for the LAN updates (atleast do this, don't pawn it to any child :P)*/
				while (waitpid (termChildPid, &childStatus, WNOHANG) == 0)
				{
					/*j router is not my next Hop towards Host Lan ID, then expect a NMR from neighbors apart from j*/
					for (i = 0; i < LANS; i++)
					{
						for (j = 0; j < ROUTERS; j++)
						{
							/*Next Hop routers towards the Host Lan Id i*/
							NMR[i][routTable[i].nextHopRouter] = 2;
							
							if (NMR[i][j] < 1 && neighRouters[j] != NULL && neighRouters[j][i].nextHopRouter == routerId)
							{
								NMR[i][j] = 0;
							}
							
						}
					}
															
					/*Check the LAN file(s) for updates related to you!*/
					for (i = 0; i < nAssLANs; i++)
					{
						sprintf (myMessage, "");
						while (numChars = read (LANfd[LAN[i]], myMessage, SIZE))
						{
							/*Check the file for data*/
							myMessage[SIZE] = '\0';
							
							tokenizer = strtok (myMessage, " ");
							strcpy (temp, tokenizer);

							/*If there is a receiver for a LAN in which I am associated, register it*/
							if (!strcmp (temp, "receiver"))
							{
								tokenizer = strtok (NULL, " ");
								garbVal = atoi (tokenizer);
								updateOnRecvCount[garbVal] = 1;
							}
							else if (!strcmp (temp, "NMR"))
							{
								tokenizer = strtok (NULL, " ");
								curLanId = atoi (tokenizer);
				
								tokenizer = strtok (NULL, " ");
								rSendingNMR = atoi (tokenizer);
				
								tokenizer = strtok (NULL, " ");
								hostLanId = atoi (tokenizer);
								
								/*Update NMR Table*/
								if (NMR[hostLanId][rSendingNMR] == 0)
									NMR[hostLanId][rSendingNMR] = 1;
									
								updateOnNMR[hostLanId][rSendingNMR] = 1;
								
								/*If there are no receivers on curLanId, stop sending messages to that LAN*/
								if (recvCount[curLanId] == 0)
									routTable[hostLanId].forwardTable[curLanId] = 0;
									
								/*Check if I should also send an NMR?*/
								shouldISend = 1;
								
								for (j = 0; j < ROUTERS; j++)
								{
									if (NMR[hostLanId][j] == 0)
									{
										shouldISend = 0;
										break;
									}
								}
								
								if (shouldISend)
								{
									for (j = 0; j < LANS; j++)
									{
										if (j != parentLAN[hostLanId] && routTable[hostLanId].forwardTable[j] == 1 && recvCount[j] == 1) /*I have receivers!!*/
											shouldISend = 0;
									}
								}
								
								sendNMR[hostLanId] = shouldISend;
								if (sendNMR[hostLanId])
								{
									/*Send that goddamn NMR!!*/
								}
							}
							else if (!strcmp (temp, "data"))
							{
								tokenizer = strtok (NULL, " ");
								curLanId = atoi (tokenizer);
				
								tokenizer = strtok (NULL, " ");
								hostLanId = atoi (tokenizer);
								
								
								canSend = 0;

								/*Conditions for forwarding..*/
								
								prevSent = 0;
								/*Initially, see if you have any associated LANs to forward*/
								for (k = 0; k < LANS; k++)
								{
									if (routTable[curLanId].forwardTable[k] == 1)
									{
										prevSent = 1;
										break;
									}
								}

								/*Need to set my forward table*/
								/*******************************
								If
								* The router is a Designated Router (cur LAN = Host LAN) OR
								* The LAN from which the message is coming is through the next Hop router towards the source
								* If the router itself is not the next hop router towards the source.
								*******************************/
								if (curLanId == hostLanId || (neighRouters[routTable[hostLanId].nextHopRouter] != NULL && neighRouters[routTable[hostLanId].nextHopRouter][curLanId].hopCount == 0 && routTable[hostLanId].nextHopRouter != routerId)) /*If I have got the message from a router next hop towards the source*/
								{								
									if (prevSent == 0)
									{
										for (l = 0; l < nAssLANs; l++)
										{
											forward = 1;
											for (k = 0; k < ROUTERS; k++)
											{
												if (neighRouters[k] != NULL && neighRouters[k][LAN[l]].hopCount == 0 && k != routerId) /*Router k is attached to LAN l*/
												{
													/*Now check if k has a shorter route to Host LAN*/
													if (neighRouters[k][hostLanId].hopCount < routTable[hostLanId].hopCount)
														forward = 0;
													else if (neighRouters[k][hostLanId].hopCount == routTable[hostLanId].hopCount && k < routerId)
														forward = 0;
												}
											}
											if (LAN[l] != curLanId && forward == 1)
												routTable[hostLanId].forwardTable[LAN[l]] = 1;
										}
									}
											
									canSend = 1;
								}
								else
									canSend = 0;
								
								/*Depending on what you have decided, send the data*/
								if (canSend)
								{
									parentLAN[hostLanId] = curLanId;
										
									/*If I am a leaf Router, check if I have to Send an NMR*/
									shouldISend = 1;
								
									for (j = 0; j < ROUTERS; j++)
									{
										if (NMR[hostLanId][j] == 0)
										{
											shouldISend = 0;
											break;
										}
									}
									
									//printf ("Router %d: Phase 1: %d\n", routerId, shouldISend);
									
									if (shouldISend)
									{
										for (j = 0; j < LANS; j++)
										{
											if (j != curLanId && routTable[hostLanId].forwardTable[j] == 1 && recvCount[j] == 1) /*I have receivers!!*/
												shouldISend = 0;
										}
									}
									//printf ("Router %d: Phase 2: %d\n", routerId, shouldISend);
									if (shouldISend)
									{
										sendNMR[hostLanId] = shouldISend;
										
										/*Send that goddamn NMR*/
										continue;
									}
									for (j = 0; j < LANS; j++)
									{
										if (routTable[hostLanId].forwardTable[j])
										{
											sprintf (fileMessage, "data %d %d ", j, hostLanId); 
											paddFixLen (fileMessage);

											write (rOUT, fileMessage, strlen (fileMessage));
										}
									}
								}
							}
							else if (!strcmp (temp, "DV"))
							{
								/*Check for the accuracy of my Routing table!*/
								tokenizer = strtok (NULL, " ");
								strcpy (med1, tokenizer);

								tokenizer = strtok (NULL, " ");
								strcpy (med2, tokenizer); /*I am least concerned about the LAN!*/
								
								if (atoi (med2) == routerId) continue; /*Own message!*/

								incRoutId = atoi (med2);

								j = 0;
								tokenizer = strtok (NULL, " ");
								while (tokenizer != NULL)
								{
									strcpy (recvDV[j++], tokenizer);
									tokenizer = strtok (NULL, " ");
								}
								
								sprintf (tempFName, "RT%d.txt", routerId);
								tempFd = open (tempFName, O_WRONLY|O_CREAT|O_TRUNC, 0777);
								
								sprintf (tempFName, "Router %d\nLID\tNHID\tHC\tL0\tL1\tL2\tL3\tL4\tL5\tL6\tL7\tL8\tL9\n", routerId);
								write (tempFd, tempFName, strlen (tempFName));
								
								for (j = 0; j < LANS; j++)
								{
									if (routTable[j].hopCount > (atoi (recvDV[2 * j]) + 1))
									{
										routTable[j].hopCount = atoi (recvDV[2 * j]) + 1;
										/*Extraction of routerID in Integer format*/
										routTable[j].nextHopRouter = recvDV[2 * j + 1][6] - 48; 
									}
									/*If there is a tie, break it using the smaller prefix*/
									if ((routTable[j].hopCount == (atoi (recvDV[2 * j]) + 1)) && ((recvDV[2 * j + 1][6] - 48) < routTable[j].nextHopRouter))
									{
										routTable[j].hopCount = atoi (recvDV[2 * j]) + 1;
										/*Extraction of routerID in Integer format*/
										routTable[j].nextHopRouter = recvDV[2 * j + 1][6] - 48; 
									}
									
									sprintf (tempFName, "%d\t%d\t%d\t", j, routTable[j].nextHopRouter, routTable[j].hopCount);
									for (k = 0; k < LANS; k++)
									{
										sprintf (med1, "%d\t", routTable[j].forwardTable[k]);
										strcat (tempFName, med1);
									}
									strcat (tempFName, "\n");
									write (tempFd, tempFName, strlen (tempFName));
								}
								
								
								/*It is a neighbor..! Steal its Routing table :P*/
								if (neighRouters[incRoutId] == NULL)
								{
									nAssRouters++;				
									neighRouters[incRoutId] = (struct RoutingTable *) malloc (LANS * sizeof (struct RoutingTable));
								}
	
								k = 0;
								for (l = 0; l < LANS; l++)
								{
									neighRouters[incRoutId][l].lanId = l;
									neighRouters[incRoutId][l].hopCount = atoi (recvDV[k++]);
									neighRouters[incRoutId][l].nextHopRouter = recvDV[k++][6] - 48;
								}

								
								close (tempFd);
								/*Updated my DV Table!*/
							}						
						}
					}
					/*I am supposed to check the LAN files ONCE in ONE Second (I believe) :P*/
					sleep(1);
				}
			}
		}
	}

	/*Stop DV Sending*/
	kill (DVChildPid, SIGTERM);
	
	/*Stop checking for receivers*/
	kill (checkRecvPid, SIGTERM);
	
	/*Stop updating NMRs*/
	kill (NMRUpdatePid, SIGTERM);
	
	/*Stop sending NMRs*/
	kill (NMRSendPid, SIGTERM);

	for (i = 0; i < nAssLANs; i++)
		close (LANfd[LAN[i]]);
	close (rOUT);
	
	for (i = 0; i < ROUTERS; i++)
	{
		if (neighRouters[i] != NULL)
			free (neighRouters[i]);
	}

	free (dv);
	free (myMessage);
	free (tempFName);

	shmdt ((void *) routTable); 
	shmctl (shmId1, IPC_RMID, NULL);

	shmdt ((void *) recvCount); 
	shmctl (shmId2, IPC_RMID, NULL);

	shmdt ((void *) updateOnRecvCount); 
	shmctl (shmId3, IPC_RMID, NULL);
	
	shmdt ((void *) sendNMR); 
	shmctl (shmId4, IPC_RMID, NULL);
	
	shmdt ((void *) parentLAN); 
	shmctl (shmId5, IPC_RMID, NULL);
	
	for (i = 0; i < LANS; i++)
	{
		shmdt ((void *) NMR[i]); 
		shmctl (shmIdNMR[i], IPC_RMID, NULL);
		
		shmdt ((void *) updateOnNMR[i]); 
		shmctl (shmIdupdateNMR[i], IPC_RMID, NULL);
	}
	
	printf ("ROUTER %d: Program Terminating\n", routerId);
	return 0;	
}
