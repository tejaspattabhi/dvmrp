#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <signal.h>

#define SIZE 120
#define STRSIZE 100

#define TRUE 1

void paddFixLen (char *);

void paddFixLen (char *message)
{
	while (strlen (message) < SIZE - 1)
		strcat (message, "_");
	strcat (message, "\n");
}

int main (int argc, char *argv[])
{
	int termChildStatus;
	pid_t termChildPid, recvChildPid;

	int recvFlag, sendFlag, lanId, hostId;
	int tos, period;
	int hIN, hOUT, LAN;
	int i;
	int numChars;
	int firstRun;

	char hinFname[STRSIZE], houtFname[STRSIZE], LANFname[STRSIZE];
	char *temp, *myMessage, *fileMessage;

	hIN = hOUT = lanId = tos = hostId = period = -1;
	recvFlag = sendFlag = numChars = 0;
	firstRun = 1;

	temp = myMessage = fileMessage = NULL;

	myMessage = (char *) malloc (sizeof (char) * (SIZE + 10));
	fileMessage = (char *) malloc (sizeof (char) * (SIZE + 10));

	if (argc < 3)
	{
		printf ("Invalid set of arguements.. Please execute again with good arguements\n");
		return 1;
	}
	
	hostId = atoi (argv[1]);
	lanId = atoi (argv[2]);
	
	if (!strcmp(argv[3], "sender"))
		sendFlag = 1;
	else if (!strcmp(argv[3], "receiver"))
		recvFlag = 1;
	else
	{
		printf ("Invalid arguement #2.. Abnormal termination!\n");
		return 1;
	}

	if (sendFlag)
	{
		if (argc < 5)
		{
			printf ("Invalid set of arguements.. Please execute again with good arguements\n");
			return 1;
		}

		tos = atoi (argv[4]);
		period = atoi (argv[5]);
	}

	if (hostId < 0 || hostId > 9 || lanId < 0 || lanId > 9)
	{
		printf ("Invalid Host or LAN ID\nAbnormal termination\n***************\n");
		return 1;
	}
	
	sprintf(houtFname, "hout%d.txt", hostId);
	sprintf(hinFname, "hin%d.txt", hostId);
	sprintf(LANFname, "lan%d.txt", lanId);

	if (recvFlag)
		hIN = open (hinFname, O_CREAT|O_WRONLY|O_TRUNC, 0777);

	hOUT = open (houtFname, O_CREAT|O_WRONLY|O_TRUNC, 0777);
	LAN = open (LANFname, O_RDONLY|O_CREAT, 0777);
	
/*	printf ("*******************\nHost Description:\n");
	printf ("Host ID: %d\nLAN ID: %d\nsendFlag: %d\nrecvFlag: %d\nTOS: %d\nPeriod: %d\n", hostId, lanId, sendFlag, recvFlag, tos, period);
	printf ("HOUT Name and ID: %s and %d\nHIN Name and ID: %s and %d\nLAN File ID: %d\n", houtFname, hOUT, hinFname, hIN, LAN);
	printf ("*******************\n");
*/
	/*Notify the router if I am a receiver*/
	if (recvFlag)
	{
		recvChildPid = fork ();
		if (recvChildPid < 0)
		{
			printf ("Error in Forking!\nAborting!!\n");
			return 1;
		}
		else if (recvChildPid == 0) /*My Child Process - Print Receiver every 10 seconds!!*/
		{
			while (TRUE)
			{
				sprintf (myMessage, "receiver %d ", lanId);
				paddFixLen (myMessage);
				write (hOUT, myMessage, strlen (myMessage));	
				sleep (20); /*Every 20 seconds, I need to prove myself that I am alive and receiving :( */
			}
		}
		/*From here, it's the parent..! Behave like a host*/
	}			

	termChildPid = fork();
	if(termChildPid < 0)
	{
		printf ("Error in forking a new process!\nPlease restart!!\n");
		return 1;
	}
	else if(termChildPid == 0)
	{
		sleep (100); /*This process will run only for 100 seconds!*/
		return 0;
	}
	else
	{
		/*Don't start till the TIME-TO-START!!*/
		if (sendFlag)
			sleep (tos); /*Works best on the assumption that tos < 100*/
			
		while(waitpid (termChildPid, &termChildStatus, WNOHANG) == 0)
		{
			/*Host Implementation*/
			if (sendFlag) /*Implementation of a sender*/
			{
				/*Send the message and wait for 'period' seconds*/
				sprintf (fileMessage, "data %d %d ", lanId, lanId);
				paddFixLen (fileMessage);
				write (hOUT, fileMessage, strlen (fileMessage));
				sleep (period);
			}
			else if (recvFlag) /*Implementation of receiver*/
			{
				/*Check if there is any new entry made in the LAN file*/
				sprintf (myMessage, "");
				if (numChars = read (LAN, myMessage, SIZE))
				{
					do 
					{
						/*I have some data in LAN file. Is it relavent to me? What am I supposed to do with it?*/
						myMessage[SIZE] = '\0';
						//printf ("HOST %d: %s\n", hostId, myMessage);
						temp = strtok (myMessage, " ");

						/*If it is data, take it*/
						if (!strcmp (temp, "data"))
						{
							strcpy (fileMessage, temp);
							strcat (fileMessage, " ");
							for (i = 1; i < 3; i++)
							{
								temp = strtok (NULL, " ");
								strcat (fileMessage, temp);
								strcat (fileMessage, " ");
							}
												
							/*I have the message, I need to officially receive it*/
							paddFixLen (fileMessage);
							//printf ("Data written into HIN: %s\n", fileMessage);
							write (hIN, fileMessage, strlen (fileMessage));
						}

					}while (numChars = read (LAN, myMessage, SIZE));
				}
			}
		}
		close (hOUT);
		close (hIN);
		close (LAN);

		/*I'm dying! So stop telling the LAN that I am receiving*/
		kill (recvChildPid, SIGTERM);		
		
		free (myMessage);
		free (fileMessage);
	}
	return 0;
}