#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <signal.h>

#define SIZE 120
#define STRSIZE 100

#define LANS 10
#define ROUTERS 10
#define HOSTS 10

void paddFixLen (char *);

void paddFixLen (char *message)
{
	while (strlen (message) < SIZE - 1)
		strcat (message, "_");
	strcat (message, "\n");
}


int main (int argc, char *argv[])
{
	int childStatus;
	pid_t termChildPid;

	int lanId;
	int host[HOSTS], router[ROUTERS], lan[LANS];
	int hostFd[HOSTS], routFd[ROUTERS], lanFd[LANS];
	int nHosts, nRouters, nLANs;
	int i, j, garbVal;

	char hostFname[STRSIZE], routFname[STRSIZE], LANFname[STRSIZE];
	char *dv, *tokenizer, *myMessage, *fileMessage;
	char *med1, *med2, *temp;

	/*Initializations!*/
	for (i = 0; i < 10; i++) /*10 would suffice :P Don't know which one to use - LANS, ROUTERS or HOSTS*/
	{
		hostFd[i] = routFd[i] = lanFd[i] = -1;
		host[i] = router[i] = lan[i] = -1;
	}

	lanId = -1;
	nHosts = nRouters = nLANs = 0;
	dv = tokenizer = myMessage = fileMessage = NULL;
	med1 = med2 = temp = NULL;

	dv = (char *) malloc (sizeof (char) * (SIZE + 10));
	myMessage = (char *) malloc (sizeof (char) * (SIZE + 10));
	fileMessage = (char *) malloc (sizeof (char) * (SIZE + 10));
	med1 = (char *) malloc (sizeof (char) * (SIZE + 10));
	med2 = (char *) malloc (sizeof (char) * (SIZE + 10));
	temp = (char *) malloc (sizeof (char) * (SIZE + 10));

	/*Process my Command Line Arguements*/

	if (strcmp (argv[1], "host"))
	{
		printf ("Host: Please enter the arguements in the order: 'host <host-id(s)> router <router-id(s)> LAN <LAN-ID's>'\n");
		printf ("Kindly note that the arguements are case sensitive!\n");
		return 1;
	}
	
	i = 2;
	while (strcmp (argv[i], "router") && i < argc)
	{
		if (!strcmp (argv[i], "lan"))
		{
			printf ("Router: Please enter the arguements in the order: 'host <host-id(s)> router <router-id(s)> LAN <LAN-ID's>'\n");
			printf ("Kindly note that the arguements are case sensitive!\n");
			return 1;
		}
		host[nHosts++] = atoi (argv[i++]);
	}
	i++;

	while (strcmp (argv[i], "lan") && i < argc)
		router[nRouters++] = atoi (argv[i++]);
	i++;

	while (i < argc)
		lan[nLANs++] = atoi (argv[i++]);		
	
	for (i = 0; i < nHosts; i++)
	{
		sprintf (hostFname, "hout%d.txt", host[i]);
		hostFd[host[i]] = open (hostFname, O_RDONLY|O_CREAT, 0777);

		if (hostFd[host[i]] < 0)
		{
			printf ("Failed to open the file for HOST %d\nAborting.... :( \n", host[i]);
			return 1;
		}
	}

	for (i = 0; i < nRouters; i++)
	{
		sprintf (routFname, "rout%d.txt", router[i]);
		routFd[router[i]] = open (routFname, O_RDONLY|O_CREAT, 0777);

		if (routFd[router[i]] < 0)
		{
			printf ("Failed to open the file for ROUTER %d\nAborting.... :( \n", host[i]);
			return 1;
		}
	}

	/*LAN Files are to be written! Not read :P*/
	for (i = 0; i < nLANs; i++)
	{
		sprintf (LANFname, "lan%d.txt", lan[i]);
		lanFd[lan[i]] = open (LANFname, O_WRONLY|O_CREAT, 0777);

		if (lanFd[lan[i]] < 0)
		{
			printf ("Failed to open the file for LAN %d\nAborting.... :( \n", lan[i]);
			return 1;
		}
	}

	/*Controller Process begins*/
	termChildPid = fork ();
	if(termChildPid < 0)
	{
		printf ("Error in forking a new process!\nPlease restart!!\n");
		return 1;
	}
	else if(termChildPid == 0)
	{
		sleep (100); /*This process will run only for 100 seconds!*/
		return 0;
	}
	else
	{
		/*printf ("*************************************************************\n");
		printf ("Controller process has started!!\n");
		printf ("The LANS registered and their FDs\n");
		printf ("LAN ID\tFD\n");
		for (i = 0; i < nLANs; i++)
			printf ("%d\t%d\n", lan[i], lanFd[lan[i]]);

		printf ("\nThe ROUTERS registered and their FDs\n");
		printf ("ROUTER_ID\tFD\n");
		for (i = 0; i < nRouters; i++)
			printf ("%d\t%d\n", router[i], routFd[router[i]]);

		printf ("\nThe HOSTS registered and their FDs\n");
		printf ("HOST_ID\tFD\n");
		for (i = 0; i < nHosts; i++)
			printf ("%d\t%d\n", host[i], hostFd[host[i]]);
		printf ("*************************************************************\n");*/

		while(waitpid (termChildPid, &childStatus, WNOHANG) == 0)
		{
			/*Controller Implementation*/
			for (i = 0; i < nHosts; i++)
			{
				if (read (hostFd[host[i]], myMessage, SIZE))
				{
					do
					{
						myMessage[SIZE] = '\0';
						strcpy (med1, myMessage);

						tokenizer = strtok (myMessage, " ");

						strcpy (med2, tokenizer); /*To get rid of the first arguement!*/
						tokenizer = strtok (NULL, " ");

						lanId = atoi (tokenizer);
						write (lanFd[lanId], med1, SIZE);

					}while (read (hostFd[host[i]], myMessage, SIZE));
				}
			}

			for (i = 0; i < nRouters; i++)
			{
				if (read (routFd[router[i]], myMessage, SIZE))
				{
					do
					{
						myMessage[SIZE] = '\0';
						strcpy (med1, myMessage);

						tokenizer = strtok (myMessage, " ");

						strcpy (med2, tokenizer); /*To get rid of the first arguement!*/
						tokenizer = strtok (NULL, " ");

						lanId = atoi (tokenizer);
						
						write (lanFd[lanId], med1, SIZE);
						
					}while (read (routFd[router[i]], myMessage, SIZE));
				}
			}
		}

		for (i = 0; i < nHosts; i++)
			close (hostFd[host[i]]);

		for (i = 0; i < nRouters; i++)
			close (routFd[router[i]]);

		for (i = 0; i < nLANs; i++)
			close (lanFd[lan[i]]);

		free (dv);
		free (myMessage);
		free (fileMessage);
		free (med1);
		free (med2);
		free (temp);
	}
	return 0;	
}
